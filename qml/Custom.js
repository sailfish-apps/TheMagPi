.pragma library

var HOST = "http://magpi-api.appspot.com"
var ISSUES_URL = HOST+"/issues"
var NEWS_URL = HOST+"/news"

var issuesPagetokens = [];

function requestURL(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url ,true);
    xhr.onreadystatechange = function()
    {
        if ( xhr.readyState == xhr.DONE)
        {
            if ( xhr.status == 200)
            {
                callback(JSON.parse(xhr.responseText))
            }
        }
    }
    xhr.send();
}

function equals(str1, str2) {
    if(typeof str2 === "undefined")
        return false

    for (var c=0; c<str1.length; c++) {
        if (str1.charCodeAt(c) != str2.charCodeAt(c)) {
            console.log('c:'+c+' '+str1.charCodeAt(c)+'!='+str2.charCodeAt(c));
            return false;
        }
    }
    return true;
}
