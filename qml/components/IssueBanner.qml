import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    id: issueBanner
    property alias text: bannerText.text

    Rectangle {
        id: bannerBackground

        anchors.fill: parent
        color: Theme.highlightColor
        opacity: 0.9
    }

    Label {
        id: bannerText
        width: parent.width
        height: Theme.itemSizeExtraSmall
        anchors {
            verticalCenter: bannerBackground.verticalCenter
            horizontalCenter: bannerBackground.horizontalCenter
        }

        font {
            pixelSize: Theme.fontSizeLarge
            bold: true
        }
        color: Theme.primaryColor
        horizontalAlignment: Text.AlignHCenter
    }
}
