/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../Custom.js" as JS
import "../components" as Components

Page {
    id: issuesPage
    property string nextPagetoken;
    allowedOrientations: Orientation.All

    BusyIndicator {
        id: pageBusy
        anchors.centerIn: parent
        running: false
    }

    SilicaFlickable {
        anchors.fill: parent

        SilicaGridView {
            id: issuesPageListView
            anchors.fill: parent
            cellWidth: isPortrait ? width/2 : width/3
            cellHeight: 500
            VerticalScrollDecorator { flickable: issuesPageListView }

            PullDownMenu {
                id: pulleyMenu
                MenuItem {
                    text: "News"
                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("Settings.qml"));
                    }
                }

                MenuItem {
                    text: "Newer issues"
                    visible: JS.issuesPagetokens.length > 0
                    onClicked: {
                        JS.issuesPagetokens.pop();
                        pageStack.clear();
                        pageStack.push(Qt.resolvedUrl("Issues.qml"));
                    }
                }
            }

            PushUpMenu {
                id: pushupMenu
                visible: false
                MenuItem {
                    text: "Older issues"
                    onClicked: {
                        JS.issuesPagetokens.push(nextPagetoken)
                        pageStack.clear();
                        pageStack.push(Qt.resolvedUrl("Issues.qml"));
                    }
                }
            }

            header: PageHeader {
                width: parent.width
                title: "Issues"
            }

            model: ListModel {
                id: issuesModel
            }

            delegate: Item {
                width: issuesPageListView.cellWidth
                height: issuesPageListView.cellHeight

                //menu: itemContextMenu

                Image {
                    id: itemImage
                    smooth: true
                    fillMode: Image.PreserveAspectFit
                    source: image_url

                    cache: true
                    sourceSize.width: issuesPageListView.cellWidth - 20
                    anchors.centerIn: parent
                    onStatusChanged: {
                        issuesPageListView.cellHeight = height + 20
                    }

                    Components.IssueBanner {
                        text: "Issue "+id
                        visible: itemImage.status == Image.Ready ? true : false
                        width: itemImage.width
                        height: Theme.itemSizeExtraSmall

                        anchors {
                            bottom: parent.bottom
                            margins: Theme.paddingLarge
                        }
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        pageBusy.running = true
                        JS.requestURL(JS.ISSUES_URL+"/"+id, function(data) {
                            pageBusy.running = false
                            pageStack.push(Qt.resolvedUrl("Issue.qml"), {issueData: data});
                        })
                    }
                }

//                Component {
//                    id: itemContextMenu
//                    ContextMenu {
//                        MenuItem {
//                            text: "Download"
//                            onClicked: {
//                                console.log("Downloading")
//                            }
//                        }
//                    }
//                }
            }
        }
    }

    Component.onCompleted: {
        var url = JS.ISSUES_URL
        var latestPage = JS.issuesPagetokens[JS.issuesPagetokens.length-1]
        if(latestPage)
            url += "?pagetoken="+latestPage

        var hasNextPage = true
        JS.requestURL(url, function(data) {
            data.issues.forEach(function(item) {
                issuesModel.append(item);
                if(item.id === 1)
                    hasNextPage = false;
            })

            if(hasNextPage) {
                pushupMenu.visible = true;
                nextPagetoken = data.pagetoken;
            }
        });
    }
}


