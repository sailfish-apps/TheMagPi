import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    property var issueData: null
    property string pdf_url: issueData.pdf_url
    id: issuePage
    allowedOrientations: Orientation.All

    SilicaFlickable {
        id: issuePageFlickable
        anchors.fill: parent
        contentHeight: issueColumn.height
        VerticalScrollDecorator { flickable: aboutPageFlickable }

        Column {
            id: issueColumn
            spacing: Theme.paddingSmall
            anchors {
                margins: Theme.paddingLarge
                left: parent.left
                right: parent.right
            }

            PageHeader {
                title: "Issue " + issueData.id
            }

            Image {
                source: issueData.image_url
                sourceSize.width: parent.width
            }

            Text {
                width: parent.width
                font.pixelSize: Theme.fontSizeExtraSmall
                wrapMode: Text.Wrap
                color: Theme.primaryColor
                textFormat: Text.RichText
                text: issueData.content
                onLinkActivated: console.log(link + " link activated")
            }

            SectionHeader {
                text: issueData.date
            }
        }
    }

    Component.onCompleted: {
        console.log(JSON.stringify(issueData))
    }
}





