# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = magPi

CONFIG += sailfishapp

SOURCES += src/magPi.cpp

OTHER_FILES += qml/magPi.qml \
    qml/cover/CoverPage.qml \
    rpm/magPi.changes.in \
    rpm/magPi.spec \
    rpm/magPi.yaml \
    translations/*.ts \
    magPi.desktop \
    qml/Custom.js \
    qml/pages/Issues.qml \
    qml/components/IssueBanner.qml \
    qml/pages/Issue.qml

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n
TRANSLATIONS += translations/magPi-de.ts

